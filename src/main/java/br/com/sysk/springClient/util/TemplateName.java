package br.com.sysk.springClient.util;

public enum TemplateName {

	LIST_TEACHERS("list_teacher"),
	ERROR("error");
	
	private String name;
	
	private TemplateName(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
