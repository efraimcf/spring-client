package br.com.sysk.springClient.util;

public enum ResourceURL {
	
	LIST_TEACHERS("/teacher/list");
	
	private String url;
	
	private ResourceURL(String url){
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}
}
