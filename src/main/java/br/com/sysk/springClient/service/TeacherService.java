package br.com.sysk.springClient.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sysk.springClient.domain.Teacher;
import br.com.sysk.springClient.util.ResourceURL;

@Service
public class TeacherService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TeacherService.class);
	
	@Autowired
	private RestClientService restService;
	
	@SuppressWarnings("unchecked")
	public List<Teacher> getTeachers() throws Exception {
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		List<Teacher> teachers = restService.doGet(ResourceURL.LIST_TEACHERS.getUrl(), List.class);
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=done");
		}
		return teachers;
	}
}
