package br.com.sysk.springClient.service;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestClientService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestClientService.class);
	
	@Value("${server.host}")
	private String host;
	
	@Value("${server.username}")
	private String username;
	
	@Value("${server.password}")
	private String password;
	
	public <T> T doGet(String method, Class<T> type) throws Exception {
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		RestTemplate restTemplate = getRestTemplate();
		HttpEntity<Object> requestEntity = getHeaders(null);
		String url = host + method;
		ResponseEntity<T> responseEntity = restTemplate.exchange(url, 
				HttpMethod.GET, requestEntity, type);
		T response = responseEntity.getBody();
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		return response;
	}

	public <T> T doPost(String method, Object data, Class<T> type) throws Exception {
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		RestTemplate restTemplate = getRestTemplate();
		HttpEntity<Object> requestEntity = getHeaders(data);
		String url = host + method;
		ResponseEntity<T> responseEntity = restTemplate.exchange(url, 
				HttpMethod.POST, requestEntity, type);
		T response = responseEntity.getBody();
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		return response;
	}
	
	private RestTemplate getRestTemplate(){
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		RestTemplate restTemplate = new RestTemplate();
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		return restTemplate;
	}
	
	private HttpEntity<Object> getHeaders(Object data){
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		String auth = username + ":" + password;
		byte[] bytes = Base64.getEncoder().encode(auth.getBytes());
		String base64 = new String(bytes);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Basic " + base64);
		HttpEntity<Object> httpEntity = data == null ?
				new HttpEntity<Object>(httpHeaders) : 
					new HttpEntity<Object>(data, httpHeaders);
		if (LOGGER.isTraceEnabled()){
			LOGGER.trace("function=getTeachers() status=init");
		}
		return httpEntity;
	}
}
