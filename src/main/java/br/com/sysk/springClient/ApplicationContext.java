package br.com.sysk.springClient;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Initialization class for Application
 * 
 * @author Efraim Coutinho Ferreira
 * @version 1.0
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableWebMvc
public class ApplicationContext extends SpringBootServletInitializer {

	public static void main( final String[] args ) {
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
		SpringApplication.run( ApplicationContext.class, args );
	}
}