package br.com.sysk.springClient.domain;

import java.util.List;

import br.com.sysk.springClient.domain.util.DomainModel;

public class Teacher extends DomainModel {

	private static final long serialVersionUID = 4691051653347402427L;

	public static final String TABLE_NAME = "teacher";

	private String name;
	
	private List<Course> courses;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
}
