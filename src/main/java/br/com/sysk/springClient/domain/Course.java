package br.com.sysk.springClient.domain;

import java.util.List;

import br.com.sysk.springClient.domain.util.DomainModel;

public class Course extends DomainModel {

	private static final long serialVersionUID = -3797673543613274115L;

	public static final String TABLE_NAME = "course";

	private String description;
	
	private Teacher teacher;
	
	private List<Student> students;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}
}
