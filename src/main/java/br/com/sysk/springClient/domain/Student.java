package br.com.sysk.springClient.domain;

import java.util.List;

import br.com.sysk.springClient.domain.util.DomainModel;

public class Student extends DomainModel {
	
	private static final long serialVersionUID = 4775978671972681275L;

	public static final String TABLE_NAME = "student";
	
	private String name;
	
	private Integer age;
	
	private String document;
	
	private String photo;
	
	private List<Course> courses;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}
	
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<Course> getCourses() {
		return courses;
	}
	
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
}
