package br.com.sysk.springClient.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.sysk.springClient.domain.Teacher;
import br.com.sysk.springClient.service.TeacherService;
import br.com.sysk.springClient.util.TemplateName;

@Controller
public class TeacherController {

	@Autowired
	private TeacherService service;
	
	@RequestMapping("/teachers")
	private String getTeachers(ModelMap model){
		String template;
		try{
			List<Teacher> teachers = service.getTeachers();
			model.put("teachers", teachers);
			template = TemplateName.LIST_TEACHERS.getName(); 
		}catch(Exception e){
			model.put("Error", e.getMessage());
			template = TemplateName.ERROR.getName();
		}
		return template;
	}
}
